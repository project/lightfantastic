<?php // $Id$ ?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="navigation_right block-<?php print $block->module ?>">
	<div class="box">
	<?php if (!empty($block->subject)): ?>
		<h2><?php print $block->subject ?></h2>
	<?php endif;?>
		<div class="content"><?php print $block->content ?></div>
	</div>
</div>
